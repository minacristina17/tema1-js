function distance(first, second){
		if(Array.isArray(first) && Array.isArray(second)){
			first = removeDuplicates(first);
			second = removeDuplicates(second);
			let rez = [...first, ...second];
			rez = removeDuplicates(rez);
			let contor = 0;
			for(let i = 0; i<first.length; i++){
				for(let j=0;j<second.length;j++){
					if(first[i] === second[j]){
						contor++;
					}
				}
			}
			return rez.length - contor;
		}
		else{
			throw new Error('InvalidType')
			
		}	
}
const removeDuplicates = (vector) => {
	result =  Array.from(new Set(vector));
	return result;
}
module.exports.distance = distance

// let vector1 = [2 ,2, 7, '1', 5];
// let vector2 = [1, 3, 4, 5];
// console.log(distance(vector1,vector2));
// let vector3 = [4,7,3,2];
// let vector4 = [1,9,8,7];
// console.log(distance(vector3,vector4));
